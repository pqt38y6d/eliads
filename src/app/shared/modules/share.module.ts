import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AddEditCampaignPopupComponent} from "../../admin/share/components/popups/add-edit-campaign-popup/add-edit-campaign-popup.component";
import {MaterialModule} from "./material.module";
import {PreviewPopupComponent} from "../../admin/share/components/popups/add-edit-campaign-popup/preview-popup/preview-popup.component";
import {AddEditScreenPopupComponent} from "../../admin/share/components/popups/add-edit-screen-popup/add-edit-screen-popup.component";
import {DeleteItemPopupComponent} from "../../admin/share/components/popups/delete-item-popup/delete-item-popup.component";



@NgModule({
  declarations: [
    PreviewPopupComponent,
    AddEditCampaignPopupComponent,
    AddEditScreenPopupComponent,
    DeleteItemPopupComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],

  entryComponents: [
    PreviewPopupComponent,
    AddEditCampaignPopupComponent,
    AddEditScreenPopupComponent,
    DeleteItemPopupComponent,
  ],
})
export class ShareModule {
}
