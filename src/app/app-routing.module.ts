import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TestComponent} from './admin/share/test/test.component';
import {AdminGuard} from "./auth/guards/admin.guard";
import {ScreenGuard} from "./auth/guards/screen.guard";


const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'users', component: TestComponent},
  {path: 'templates', component: TestComponent},
  {path: 'tags', component: TestComponent},
  {path: 'settings', component: TestComponent},
  {
    path: 'auth',
    loadChildren: () => import('./auth/modules/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/modules/admin.module').then(m => m.AdminModule),
    canActivate: [AdminGuard]
  },
  {
    path: 'screen-user',
    loadChildren: () => import('./screen-user/modules/screen-user.module').then(m => m.ScreenUserModule),
    canActivate: [ScreenGuard]
  },




  // {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
