import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ScreenUserRoutingModule} from './screen-user-routing.module';
import {ScreenUserComponent} from "../screen-user.component";
import {MatCarouselModule} from '@ngmodule/material-carousel';
import {MaterialModule} from "../../shared/modules/material.module";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [ScreenUserComponent],
    imports: [
        CommonModule,
        ScreenUserRoutingModule,
        MatCarouselModule.forRoot(),
        MaterialModule,
        FormsModule
    ]
})
export class ScreenUserModule { }
