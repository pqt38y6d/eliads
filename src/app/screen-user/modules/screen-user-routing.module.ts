import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestComponent} from "../../admin/share/test/test.component";
import {ScreenUserComponent} from "../screen-user.component";


const routes: Routes = [
  {path: 'campaigns', component: ScreenUserComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScreenUserRoutingModule { }
