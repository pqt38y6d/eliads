import { Component, OnInit } from '@angular/core';
import {ScreenRestService} from "./services/screen-rest.service";
import {AuthService} from "../auth/services/auth.service";

@Component({
  selector: 'app-screen-user',
  templateUrl: './screen-user.component.html',
  styleUrls: ['./screen-user.component.scss']
})
export class ScreenUserComponent implements OnInit {

  campaigns;
  showingTimeValue = 20;
  transitionTimeValue = 250;

  constructor(private screenRestService: ScreenRestService,
              private authService: AuthService) { }

  ngOnInit() {
    this.campaigns = this.screenRestService.getAllCampaigns();
  }

}
