import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Campaign} from "../../admin/share/models/campaign";

@Injectable({
  providedIn: 'root'
})
export class ScreenRestService {

  BASE_URL = '/api/v1';

  constructor(private httpClient: HttpClient) {
  }

  getAllCampaigns(): Observable<Campaign[]> {
    return this.httpClient.get<Campaign[]>(`${this.BASE_URL}/screenUser/campaigns`);
  }
}
