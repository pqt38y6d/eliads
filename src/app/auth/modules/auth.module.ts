import {NgModule} from '@angular/core';
import {AuthComponent} from "../auth.component";
import {AuthRoutingModule} from "./auth-routing.module";
import {ShareModule} from "../../shared/modules/share.module";
import {NgxMaskModule} from "ngx-mask";


@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    AuthRoutingModule,
    ShareModule,
    NgxMaskModule.forRoot(),
  ],
  providers: []
})

export class AuthModule {
}
