import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  mail = '345@gmail.com';
  pass = '123456';
  isScreen = false;
  user = 'screen';
  screenId = '3215';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit(contactForm: NgForm) {
    let authReq = contactForm.form.value;
    if (this.isScreen) {
      const screenInf = authReq.screenID;
      authReq = {
        office: screenInf.slice(0,1),
        floor: screenInf.slice(1,2),
        room: screenInf.slice(2),
      }
    }
    authReq.isScreen = this.isScreen;
    this.authService.login(authReq);
  }

  switchToAnotherLogin() {
    this.isScreen = !this.isScreen;
    if (this.isScreen) {
      this.user = 'administrator'
    } else {
      this.user = 'screen'
    }
  }
}
