import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient,
              private router: Router,
              private toastr: ToastrService) {
  }

  public getToken() {
    return localStorage.getItem('token') ?
      localStorage.getItem('token') :
      '';
  }

  private isAuthenticated = true;
  private isAdmin = true;

  loginRequest(credentials: any) {
    return this.httpClient.post('api/v1/login', credentials, {observe: "response"});
  }


  login(credentials: any) {
    this.loginRequest(credentials).subscribe(res => {
        if (res.status == 200) {
          this.setLocalStorage(res.body);
          this.isAuthenticated = true;
          if (res.body.hasOwnProperty('user')) {
            this.isAdmin = true;
            this.router.navigate(['/admin/campaigns']).then(r => {
            });
          } else {
            this.isAdmin = false;
            this.router.navigate(['/screen-user/campaigns']).then(r => {
            })
          }
        }
      },
      error => {
        this.toastr.error(error.error, error.status);
      });
  }


  logout() {
    this.isAuthenticated = false;
    this.isAdmin = false;
    this.router.navigateByUrl('/auth').then(r => {
    });
    localStorage.clear();
  }

  isLoggedIn() {
    return this.isAuthenticated;
  }

  isAdminIn() {
    return this.isAdmin;
  }

  setLocalStorage(response: Object) {
    // @ts-ignore
    const {user, token} = response;

    localStorage.setItem('token', token);
    for (let item in user) {
      localStorage.setItem(item, user[item])
    }
  }
}
