import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CampaignsRoutingModule} from './campaigns-routing.module';
import {CampaignsComponent} from "../campaigns.component";
import {AboutCampaignComponent} from "../about-campaign/about-campaign.component";
import {ShareModule} from "../../../../shared/modules/share.module";


@NgModule({
  declarations: [
    CampaignsComponent,
    AboutCampaignComponent
  ],
  imports: [
    CampaignsRoutingModule,
    ShareModule
  ]
})
export class CampaignsModule { }
