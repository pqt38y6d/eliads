import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CampaignsComponent} from "../campaigns.component";
import {AboutCampaignComponent} from "../about-campaign/about-campaign.component";


const routes: Routes = [
  {path: '', component: CampaignsComponent},
  {path: ':id', component: AboutCampaignComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignsRoutingModule { }
