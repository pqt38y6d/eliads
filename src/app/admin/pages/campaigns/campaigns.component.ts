import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Campaign} from '../../share/models/campaign';
import {ReduxService} from "../../share/services/redux.service";
import {SelectionModel} from "@angular/cdk/collections";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {RestService} from "../../share/services/rest.service";
import {merge, of, Subscription} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit, AfterViewInit, OnDestroy {


  displayedColumns: string[] = ['select', 'name', 'tag', 'startDate', 'endDate', 'designType'];
  dataSource: Campaign[]  = [];
  selection = new SelectionModel<Campaign>(true, []);
  updateTableSubscription: Subscription;



  resultsLength: number = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private missionService: ReduxService,
              private restService: RestService,
              private headerEditorService: ReduxService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.headerEditorService.setCurrentPage('campaigns');
    this.headerEditorService.headerTitle = 'campaigns';
    this.headerEditorService.addButtonTitle = 'campaign';
    this.updateTableSubscription = this.headerEditorService.updateTable$.subscribe(
      () => {
        this.selection.clear();
        this.tableInit();
      });
  }

  ngAfterViewInit(): void {
    this.tableInit();
  }



  tableInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.restService!.getCampaignsByPage(
            this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          // @ts-ignore
          this.resultsLength = data.headers.get('X-total-count');
          return data.body;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return of([]);
        })
      ).subscribe(data => {
        console.log(data);
      this.dataSource = data;
    });
  }




  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel() {
    this.headerEditorService.deleteItems(this.selection.selected);
  }

  ngOnDestroy(): void {
    this.updateTableSubscription.unsubscribe();
  }

}
