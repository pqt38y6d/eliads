import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReduxService} from "../../../share/services/redux.service";
import {RestService} from "../../../share/services/rest.service";
import {Design} from "../../../share/models/design";
import {Subscription} from "rxjs";
import {Campaign} from "../../../share/models/campaign";

@Component({
  selector: 'app-about-campaign',
  templateUrl: './about-campaign.component.html',
  styleUrls: ['./about-campaign.component.scss']
})
export class AboutCampaignComponent implements OnInit, OnDestroy {

  id = this.activatedRoute.snapshot.params.id;
  campaign: Campaign;
  design: Design;
  fontFace: string;
  fontColor: string;
  background: string;
  private updateTableSubscription: Subscription;




  constructor(private activatedRoute: ActivatedRoute,
              private headerEditorService: ReduxService,
              private restService: RestService) { }

  ngOnInit() {
    this.updateTableSubscription = this.headerEditorService.updateCampaign$.subscribe(
      (campaign) => {
        this.campaign = campaign;
        this.initInformation();
      });

    this.restService.getCampaignById(+this.id).subscribe(campaign => {
      this.campaign = campaign;
      this.initInformation();
    });
  }

  initInformation() {
    this.fontFace = this.campaign.design.fontFace;
    this.fontColor = this.campaign.design.fontColor;
    if (this.campaign.design.designType === 'white background') {
      this.background = 'white';
    } else if (this.campaign.design.designType === 'solid color') {
      this.background = this.campaign.design.backgroundColor;
    } else if (this.campaign.design.designType === 'gradient') {
      this.background = `linear-gradient(to bottom right, ${this.campaign.design.leftColor}, ${this.campaign.design.rightColor})`
    } else if (this.campaign.design.designType === 'image') {
      this.background = `url('${this.campaign.design.image}')`
    }
    this.headerEditorService.setCurrentPage(`aboutCampaign/${this.id}`);
    this.headerEditorService.headerTitle = this.campaign.name;
    this.headerEditorService.addButtonTitle = '';
  }

  ngOnDestroy(): void {
    this.updateTableSubscription.unsubscribe();
  }

}
