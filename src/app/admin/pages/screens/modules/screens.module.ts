import {NgModule} from '@angular/core';
import {ScreensRoutingModule} from './screens-routing.module';
import {ShareModule} from "../../../../shared/modules/share.module";
import {ScreensComponent} from "../screens.component";


@NgModule({
  declarations: [
    ScreensComponent
  ],
  imports: [
    ScreensRoutingModule,
    ShareModule
  ]
})
export class ScreensModule { }
