import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {RestService} from "../../share/services/rest.service";
import {merge, of, Subscription} from "rxjs";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {SelectionModel} from "@angular/cdk/collections";
import {ReduxService} from "../../share/services/redux.service";
import {Campaign} from "../../share/models/campaign";
import {AddEditCampaignPopupComponent} from "../../share/components/popups/add-edit-campaign-popup/add-edit-campaign-popup.component";
import {MatDialog} from "@angular/material/dialog";
import {AddEditScreenPopupComponent} from "../../share/components/popups/add-edit-screen-popup/add-edit-screen-popup.component";


@Component({
  selector: 'app-screens',
  templateUrl: './screens.component.html',
  styleUrls: ['./screens.component.scss']
})
export class ScreensComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['select', 'name', 'officeId', 'floor', 'room'];
  dataSource: Screen[];
  selection = new SelectionModel<Screen>(true, []);
  updateTableSubscription: Subscription;


  resultsLength: number = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private missionService: ReduxService,
              private restService: RestService,
              private headerEditorService: ReduxService) {
  }

  ngOnInit(): void {
    this.headerEditorService.setCurrentPage('screens');
    this.headerEditorService.headerTitle = 'screens';
    this.headerEditorService.addButtonTitle = 'screen';
    this.updateTableSubscription = this.headerEditorService.updateTable$.subscribe(
      () => {
        this.selection.clear();
        this.tableInit();
      });
  }

  ngAfterViewInit(): void {
    this.tableInit();
  }


  tableInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.restService!.getScreensByPage(
            this.paginator.pageIndex, this.paginator.pageSize, this.sort.active, this.sort.direction);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          console.log(data);
          this.resultsLength = +data.headers.get('X-total-count');
          return data.body;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return of([]);
        })
      ).subscribe(data => {
      this.dataSource = data;
    });
  }

  openEditDialog(id: number) {
    const dialogRef = this.dialog.open(AddEditScreenPopupComponent, {
      width: 'auto',
      height: '90%',
      data: {message: `edit screen`, id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.restService.updateCampaign(result).subscribe(data => {
        //   this.headerEditorService.updateCampaign(data);
        // });
      }
    });
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel() {
    this.headerEditorService.deleteItems(this.selection.selected);
  }

  ngOnDestroy(): void {
    this.updateTableSubscription.unsubscribe();
  }
}
