export class Design {
  designType: string;
  fontFace: string;
  fontColor: string;
  // name: string;
  leftColor?: string;
  rightColor?: string;
  backgroundColor?: string;
  image?: string;
}
