import {Tag} from './tag';
import {Design} from './design';

export class Campaign {
  id?: number;
  name: string;
  text?: string;
  status: string;
  startDate: Date;
  endDate: Date;
  design?: Design;
  tag?: Tag;
  screens: Screen[];
  userId: number;
}

