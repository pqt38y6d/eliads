export class Template {
  name: string;
  imageURL: string;
  fontFace: string;
  fontColor: string;
}
