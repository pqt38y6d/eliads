import {Office} from "./office";

export class Screen {
  // [x: string]: any;
  id: number;
  name: string;
  officeId?: number;
  floor: number;
  room: number;
  office: Office;
}

