import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AddEditCampaignPopupComponent} from '../popups/add-edit-campaign-popup/add-edit-campaign-popup.component';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {ReduxService} from "../../services/redux.service";
import {RestService} from "../../services/rest.service";
import {Location} from '@angular/common';
import {DeleteItemPopupComponent} from "../popups/delete-item-popup/delete-item-popup.component";
import {AddEditScreenPopupComponent} from "../popups/add-edit-screen-popup/add-edit-screen-popup.component";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  deleteButtonSubscription: Subscription;
  getCurrentPageSubscription: Subscription;
  itemsForDelete: any[] = [];
  title: string;
  currentPageInformation: string = '';

  @Output() onChanged = new EventEmitter<boolean>()

  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private reduxService: ReduxService,
              private restService: RestService,
              private location: Location) {

  }

  ngOnInit() {
    this.deleteButtonSubscription = this.reduxService.campaignsDeleted$.subscribe(
      campaigns => {
        this.itemsForDelete = campaigns;
      });

    this.getCurrentPageSubscription = this.reduxService.currentPage$.subscribe(
      pageInf => {
        this.currentPageInformation = pageInf;
      }
    )
  }

  async openAddNewDialog() {
    let dialog: any;
    let tags;
    let data;
    if (this.reduxService.addButtonTitle === 'campaign') {
      dialog = AddEditCampaignPopupComponent;
      tags = await this.restService.getAllTags().toPromise();
      data = {message: `new ${this.reduxService.addButtonTitle}`, tags}
    } else if (this.reduxService.addButtonTitle === 'screen') {
      dialog = AddEditScreenPopupComponent;
    }

    const dialogRef = this.dialog.open(dialog, {
      width: 'auto',
      height: 'auto',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restService.addNewCampaign(result).subscribe(data => {
          this.reduxService.updateTable();
        });
      }
    });
  }

  openDeleteDialog(items: any): void {
    let message: string;
    if (this.currentPageInformation === 'screens') {
      message = 'delete screens';
    } else if (this.currentPageInformation === 'campaigns') {
      message = 'delete campaigns';
    }
    const dialogRef = this.dialog.open(DeleteItemPopupComponent, {
      width: 'auto',
      height: 'auto',
      data: {message: message, items}
    });

    dialogRef.afterClosed().subscribe(result => {
      let {route, items} = result;
      let ids = [];
      items.map(item => {
        ids.push(item.id);
      });
      this.restService.deleteItem(route, ids).subscribe(() => {
        this.reduxService.updateTable();
      });
    });
  }

  async openEditDialog() {
    let dialog;
    let tags;
    let campaign;
    let data;

    const currentID = +this.currentPageInformation.slice(this.currentPageInformation.indexOf('/') + 1);

    // if (this.reduxService.addButtonTitle === 'campaign') {
    dialog = AddEditCampaignPopupComponent;
    tags = await this.restService.getAllTags().toPromise();
    campaign = await this.restService.getCampaignById(currentID).toPromise();
    data = {message: `edit campaign`, tags, campaign, currentID}
    // }

    const dialogRef = this.dialog.open(dialog, {
      width: 'auto',
      height: '90%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.restService.updateCampaign(result.item, result.id).subscribe(data => {
          this.reduxService.updateCampaign(data);
        });
      }
    });
  }

  openSidebar() {
    this.onChanged.emit();
  }

  goBack() {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.deleteButtonSubscription.unsubscribe();
    this.getCurrentPageSubscription.unsubscribe();
  }
}
