import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {RestService} from "../../../services/rest.service";
import {PreviewPopupComponent} from "./preview-popup/preview-popup.component";
import {Campaign} from "../../../models/campaign";
import {Tag} from "../../../models/tag";
import {of} from "rxjs";


@Component({
  selector: 'app-popup',
  templateUrl: './add-edit-campaign-popup.component.html',
  styleUrls: ['./add-edit-campaign-popup.component.scss']
})
export class AddEditCampaignPopupComponent implements OnInit {

  designTypes: string[] = ['white background', 'solid color', 'gradient', 'image'];
  fontFaces: string[] = ['Arial Black', 'Impact', 'Times New Roman', 'Mongolian Baiti'];
  offices = ['ALL', 1, 2, 3];
  tags: Tag[];
  newCampaignForm: FormGroup;
  selectedDesign: string = this.designTypes[0];
  selectedOffice = this.offices[0];
  floors;
  rooms;
  screens;
  minDate = new Date();
  maxDate = new Date(this.minDate.getFullYear() + 1, 11, 31);
  userID: number = +localStorage.getItem('id');
  floor: any;
  isCurrentScreensPanelDisabled = false;


  constructor(public dialogRef: MatDialogRef<AddEditCampaignPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
              private restService: RestService,
              public dialog: MatDialog) {
  }


  ngOnInit() {
    let {message, tags, campaign} = this.data;
    this.tags = tags;
    this.newCampaignForm = this.initCampaignForm();

    if (message === 'edit campaign') {
      this.screens = campaign.screens;
      this.selectedDesign = campaign.design.designType;
      this.changeDesign(this.selectedDesign);
      this.initCampaignFormForEditing(campaign);
    }
  }


  initCampaignForm() {
    return this.formBuilder.group({
      userId: [this.userID],
      name: ['Default name', [
        Validators.required
      ]],
      tag: [this.tags[0].name],
      text: ['Ублюдок, мать твою, а ну иди сюда говно собачье, решил ко мне лезть? Ты, засранец вонючий, мать твою, а? Ну иди сюда, попробуй меня трахнуть, я тебя сам трахну ублюдок, онанист чертов', [
        Validators.required]
      ],
      startDate: [this.minDate, [
        Validators.required
      ]],
      endDate: ['', [
        Validators.required
      ]],
      design:
        this.formBuilder.group({
          designType: ['white background'],
          fontFace: [this.fontFaces[0]],
          fontColor: ['#f2c94c']
        }),
      screen:
        this.editScreenPart()
    });
  }

  initCampaignFormForEditing(campaign: Campaign) {
    this.newCampaignForm.patchValue(campaign);
  }

  logFormValue() {
    console.log(this.newCampaignForm.value);
  }

  editScreenPart() {
    return this.formBuilder.group({
      office: [this.selectedOffice]
    })
  }

  changeDesign(designType: string) {
    this.newCampaignForm.removeControl('design');

    if (designType === 'solid color') {
      this.newCampaignForm.setControl('design', this.formBuilder.group({
          designType: ['solid color'],
          backgroundColor: ['#ef0d33'],
          fontFace: [this.fontFaces[0]],
          fontColor: ['#f2c94c']
        })
      )
    } else if (designType === 'white background') {
      this.newCampaignForm.setControl('design', this.formBuilder.group({
          designType: ['white background'],
          fontFace: [this.fontFaces[0]],
          fontColor: ['#f2c94c']
        })
      )
    } else if (designType === 'gradient') {
      this.newCampaignForm.setControl('design', this.formBuilder.group({
          designType: ['gradient'],
          leftColor: ['#fb466c'],
          rightColor: ['#405efa'],
          fontFace: [this.fontFaces[0]],
          fontColor: ['#f2c94c']
        })
      )
    } else if (designType === 'image') {
      this.newCampaignForm.setControl('design', this.formBuilder.group({
          designType: ['image'],
          image: [''],
          fontFace: [this.fontFaces[0]],
          fontColor: ['#f2c94c']
        })
      )
    }

  }


  openPreview(campaign: object): void {
    this.dialog.open(PreviewPopupComponent, {
      width: '70%',
      height: '90%',
      data: campaign
    });
  }

  changeOffice(office: any) {
    console.log(office);
    if (office !== 'ALL') {
        this.restService.getFloors(office).subscribe(async floors => {
          this.floors = floors;
          await (this.newCampaignForm.controls['screen'] as FormGroup).addControl('floor', new FormControl(this.floors[0], Validators.required))
        });
    } else {
      if ((this.newCampaignForm.controls['screen'] as FormGroup).controls['floor']) {
        (this.newCampaignForm.controls['screen'] as FormGroup).removeControl('floor');
      }
      if ((this.newCampaignForm.controls['screen'] as FormGroup).controls['rooms']) {
        (this.newCampaignForm.controls['screen'] as FormGroup).removeControl('rooms');
      }
    }
  }

  changeFloor(office: number, floor: any) {
    if (floor !== 'ALL') {
        this.restService.getRooms(office, floor).subscribe(async rooms => {
          this.rooms = rooms;
          await (this.newCampaignForm.controls['screen'] as FormGroup).addControl('rooms', new FormControl(this.rooms[0], Validators.required))
        });
    } else if ((this.newCampaignForm.controls['screen'] as FormGroup).controls['rooms']) {
      (this.newCampaignForm.controls['screen'] as FormGroup).removeControl('rooms');
    }
  }

  changeCurrentScreensPanelStatus() {
    if (this.selectedOffice === `ALL`) this.isCurrentScreensPanelDisabled = false;
    else this.isCurrentScreensPanelDisabled = true;

    console.log(this.isCurrentScreensPanelDisabled);
  }

}
