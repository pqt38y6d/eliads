import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AddEditCampaignPopupComponent} from "../add-edit-campaign-popup.component";
import {Campaign} from "../../../../models/campaign";

@Component({
  selector: 'app-preview-popup',
  templateUrl: './preview-popup.component.html',
  styleUrls: ['./preview-popup.component.scss']
})
export class PreviewPopupComponent implements OnInit {
  background: string;
  fontFace: string;
  fontColor: string;

  constructor(public dialogRef: MatDialogRef<PreviewPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Campaign,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.fontFace = this.data.design.fontFace;
    this.fontColor = this.data.design.fontColor;
    if (this.data.design.designType === 'white background') {
      this.background = 'white';
    } else if (this.data.design.designType === 'solid color') {
      this.background = this.data.design.backgroundColor;
    } else if (this.data.design.designType === 'gradient') {
      this.background = `linear-gradient(to bottom right, ${this.data.design.leftColor}, ${this.data.design.rightColor})`
    } else if (this.data.design.designType === 'image') {
      this.background = `url('${this.data.design.image}')`
    }
  }
}
