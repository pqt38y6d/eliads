import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {FormBuilder} from "@angular/forms";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-delete-item-popup',
  templateUrl: './delete-item-popup.component.html',
  styleUrls: ['./delete-item-popup.component.scss']
})
export class DeleteItemPopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
              private restService: RestService) {
  }

  ngOnInit() {
  }

}
