import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Campaign} from "../../../models/campaign";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {RestService} from "../../../services/rest.service";

@Component({
  selector: 'app-add-edit-screen-popup',
  templateUrl: './add-edit-screen-popup.component.html',
  styleUrls: ['./add-edit-screen-popup.component.scss']
})
export class AddEditScreenPopupComponent implements OnInit {

  newScreenForm: FormGroup;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
              private restService: RestService) {
  }

  ngOnInit(): void {

  }

  initScreenForm() {
    return this.formBuilder.group({
      name: ['Default name', [
        Validators.required
      ]],
      Office: ['Dzerzhinskogo', [
        Validators.required
      ]],
      floor: [],
      room: []
    });
  }

  initScreenFormForEditing(screen: Screen) {
    this.newScreenForm.patchValue(screen);
    // this.newCampaignForm.setControl('id', new FormControl(campaign.id));
  }

}
