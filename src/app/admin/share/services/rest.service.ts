import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Campaign} from "../models/campaign";
import {Tag} from "../models/tag";

@Injectable({
  providedIn: 'root'
})
export class RestService {
  BASE_URL = '/api/v1';

  constructor(private httpClient: HttpClient) {
  }

  getCampaignsByPage(page: number, size: number, sort: string, order: string): Observable<HttpResponse<Campaign[]>> {
    return this.httpClient.get<Campaign[]>(`${this.BASE_URL}/campaigns`,
      {
        params: new HttpParams()
          .set('page', page.toString())
          .set('size', size.toString())
          .set('sort', sort)
          .set('order', order),
        observe: "response"
      })
  }

  deleteItem(route: string, ids: number[]) {
    return this.httpClient.post(`${this.BASE_URL}/${route}/delete`, ids);
  }


  addNewCampaign(campaign: Campaign) {
    return this.httpClient.post<Campaign>(`${this.BASE_URL}/campaigns`, campaign);
  }


  getCampaignById(id: number): Observable <Campaign> {
    return this.httpClient.get<Campaign>(`${this.BASE_URL}/campaigns/${id}`);
  }


  updateCampaign(campaign: Campaign, id: number) {
    return this.httpClient.put<Campaign>(`${this.BASE_URL}/campaigns/${id}`, campaign);
  }

  getAllTags(): Observable<Tag[]> {
    return this.httpClient.get<Tag[]>(`${this.BASE_URL}/tags`);
  }

  getScreensByPage(page: number, size: number, sort: string, order: string): Observable<HttpResponse<Screen[]>> {
    return this.httpClient.get<Screen[]>(`${this.BASE_URL}/screens`,
      {
        params: new HttpParams()
          .set('page', page.toString())
          .set('size', size.toString())
          .set('sort', sort)
          .set('order', order),
        observe: "response"
      })
  }

  getFloors(office: number) {
    return this.httpClient.get(`${this.BASE_URL}/screens/floors`, {
      params: new HttpParams().set('office', office.toString())
    })
  }

  getRooms(office: number, floor: number) {
    return this.httpClient.get(`${this.BASE_URL}/screens/rooms`, {
      params: new HttpParams()
        .set('office', office.toString())
        .set('floor', floor.toString())
    })
  }


}
