import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {Campaign} from "../models/campaign";

@Injectable({
  providedIn: 'root'
})
export class ReduxService {

  constructor() { }

  headerTitle: string;
  addButtonTitle: string;

  // Observable string sources
  private campaignsDeletedSource = new Subject<Campaign[]>();
  private screensDeletedSource = new Subject<Screen[]>();
  private updateTableSource = new Subject<void>();
  private currentPageSource = new Subject<string>();
  private updateCampaignSource = new Subject<Campaign>();

  // Observable string streams
  campaignsDeleted$ = this.campaignsDeletedSource.asObservable();
  screensDeleted$ = this.screensDeletedSource.asObservable();
  updateTable$ = this.updateTableSource.asObservable();
  currentPage$ = this.currentPageSource.asObservable();
  updateCampaign$ = this.updateCampaignSource.asObservable();


  // Service message commands
  deleteItems(items: any[]) {
    this.campaignsDeletedSource.next(items);
  }

  deleteScreens(screens: Screen[]) {
    this.screensDeletedSource.next(screens);
  }

  updateTable() {
    this.updateTableSource.next();
  }

  setCurrentPage (pageInf: string) {
    this.currentPageSource.next(pageInf);
  }

  updateCampaign(campaign: Campaign) {
    this.updateCampaignSource.next(campaign)
  }
}
