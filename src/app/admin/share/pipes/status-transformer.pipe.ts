import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusTransformer'
})
export class StatusTransformerPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return value ? 'Active' : 'Deactivated';
  }

}
