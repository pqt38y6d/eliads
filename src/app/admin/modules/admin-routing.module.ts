import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminComponent} from "../admin.component";


const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      {
        path: 'campaigns',
        loadChildren: () => import('../pages/campaigns/modules/campaigns.module').then(m => m.CampaignsModule),
      },
      {
        path: 'screens',
        loadChildren: () => import('../pages/screens/modules/screens.module').then(m => m.ScreensModule),
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
