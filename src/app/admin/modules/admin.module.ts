import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from '../admin.component';
import {HeaderComponent} from "../share/components/header/header.component";
import {SidebarComponent} from "../share/components/sidebar/sidebar.component";
import {ShareModule} from "../../shared/modules/share.module";


@NgModule({
  declarations: [
    AdminComponent,
    HeaderComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ShareModule
  ]
})
export class AdminModule {
}
