import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StatusTransformerPipe} from './admin/share/pipes/status-transformer.pipe';
import {TestComponent} from './admin/share/test/test.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserModule, HAMMER_GESTURE_CONFIG} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TokenInterceptor} from "./auth/services/token.interceptor";
import {AdminGuard} from "./auth/guards/admin.guard";
import {ToastrModule} from "ngx-toastr";
import {NgxUiLoaderConfig, NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule} from "ngx-ui-loader";
import { ScreenUserComponent } from './screen-user/screen-user.component';
import {ScreenGuard} from "./auth/guards/screen.guard";
import {GestureConfig} from "@angular/material/core";

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "bgsColor": "#f2c94c",
  "bgsOpacity": 0.8,
  "bgsPosition": "center-center",
  "bgsSize": 50,
  "bgsType": "rectangle-bounce",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#ef0d33",
  "fgsPosition": "center-center",
  "fgsSize": 70,
  "fgsType": "square-loader",
  "gap": 63,
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(44,51,63,0.78)",
  "pbColor": "red",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
};


@NgModule({
  declarations: [
    AppComponent,
    StatusTransformerPipe,
    TestComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    AdminGuard,
    ScreenGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
